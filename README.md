# Affiliation

In this repository, we keep the list of affiliations of each team member, as they should appear in papers and presentations.

> Please be careful. It is possible that CRIStAL reduces the funds to the team because of the errors in affiliation.

## Two Types of Affiliations

In fact,there are only two types of affiliations for everyone who works in our team.

### Type 1. People who are paid by University of Lille, Centrale Lille, or CNRS

**Affiliation:** Univ. Lille, **CNRS, Inria**, Centrale Lille, UMR 9189 CRIStAL, F-59000 Lille, France

Permanent members in this category:

- Anne Etien
- Nicolas Anquetil
- Vincent Aranega
- Guillermo Polito

###  Type 2. People who are paid by Inria

**Affiliation:** Univ. Lille, **Inria, CNRS**, Centrale Lille, UMR 9189 CRIStAL, F-59000 Lille, France

Permanent members in this category:

- Stéphane Ducasse
- Marcus Denker
- Steven Costiou

Research engineers:

- Pablo Tesone
- Esteban Lorenzano
- Christophe Demarey
- Sebastian Jordan Montaño
- Nicolas Mauricio Rainhart
- Nahuel Palumbo
- Milton Mamani Torres
- Clotilde Toullec
- Soufyane Labsari

## PhD Students

- If paid by the University of Lille - use affiliation Type 1
- If paid by Inria - use affiliation Type 2
- If paid by another institution (e.g. Cifre PhD or another university) - use affiliation Type 2, preceeded by the name of your employer

For example:
_[company name]_, Univ. Lille, Inria, CNRS, Centrale Lille, UMR 9189 CRIStAL, F-59000 Lille, France
